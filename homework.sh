#!/bin/bash
echo "Enter the subnet Example: 192.168.1"
read subnet
echo "Enter the port number to scan:"
read port
echo "Scanning..."
for ip in $subnet.{0..255}
do
	#telnet komandasi icra olunur 0.01 saniyeden sonra sona chatdirilir ve outputda Connected sozu varsa status deyishenine assign olunur
	status=$(echo quit | timeout --signal=9 0.01 telnet $ip $port 2>/dev/null | grep -o Connected)
	#status deyisheninin deyeri Connected olduqda ip ekrana chixarilir
	if [ "$status" == "Connected" ]
		then
		   echo $ip
	fi
done
